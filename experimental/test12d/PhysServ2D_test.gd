extends Callable
## Very Experimental 2022-09-22 - NBD


# Physics2DServer expects references to be kept around.
var body : RID
var shape : RID


func _body_moved(state, index) -> void:
	# Created your own canvas item, use it here.
	RenderingServer.canvas_item_set_transform(canvas_item, state.transform)


func _ready() -> void:
	# Create the body.
	body = PhysicsServer2D.body_create()
	PhysicsServer2D.body_set_mode(body, PhysicsServer2D.BODY_MODE_RIGID)
	# Add a shape.
	shape = PhysicsServer2D.rectangle_shape_create()
	# Set rectangle extents.
	PhysicsServer2D.shape_set_data(shape, Vector2(10, 10))
	# Make sure to keep the shape reference!
	PhysicsServer2D.body_add_shape(body, shape)
	# Set space, so it collides in the same space as current scene.
	PhysicsServer2D.body_set_space(body, get_world_2d().space)
	# Move initial position.
	PhysicsServer2D.body_set_state(body, PhysicsServer2D.BODY_STATE_TRANSFORM, Transform2D(0, Vector2(10, 20)))
	# Add the transform callback, when body moves
	# The last parameter is optional, can be used as index
	# if you have many bodies and a single callback.
	PhysicsServer2D.body_set_force_integration_callback(body, self, "_body_moved", 0)
