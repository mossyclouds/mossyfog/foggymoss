!!! Remember to change the LICENSE.md if you are NOT releasing under the 
MD0 license (https://gitlab.com/GPDMCC/MD0)

---

# Title
- Game Name
- Company/Solo Name
- Your Name
- Writer's Name
- Any Important Licenses

# Overview
- High Concept (Five Sentences or Less)
- Summary (Up to Five Paragraphs)
- Genre and Target Audience
- Gameflow (Overview)
- Look and Feel
- External Influences
- Monetization

# Story
- Back Story
- Main Story
- Side Stories (List one by one)
- Story Progression

# Settings
- Setting Overview
- Game World (Include World Building)
- Look and Feel
- Visual and Aural Style/Themes
- Cinematics

# Characters
- Main Characters
- Supporting Characters
- Antagonists
- Background Characters
\* Each should have Back Story, Personality, Appearance, Story Connection, Relationships, Abilities, Location, Schedules.

---

# Gameplay
- Game Progression (Break into Sections)
- Objectives (Per Section)
- Mission Structure
- Puzzle Structure
- Balancing (Describe, can link to Spreadsheets)

# Mechanics
- Physics and Controls
- Navigation in World
- Interactivity
- Combat and Challenge Resolution
- In-Game Economy

# Game Options
-  Main Menu Options
-  Graphics and Audio Settings
-  In-Game Options
-  Pause Menu
-  When Can Change?

# Saving and Loading
- Loading from Main Menu
- Saving or Loading within Game
- Autosaving Quicksaving
- Save Management
- Cloud Backup

# Replaying
- What makes game replayable?
- Player Choice and Outcome
- Multiple Paths (More Detail)
- Multiple Endings (More Detail)
- New Game+

---

# Cheats
- Developer Cheats
- Intentional Cheats
- Possible Exploits

# Easter Eggs
- Easter Egg List
- How To Find

# Tutorial Level
- Synopsis
- Introduction
- Physical Description
- Level Flow
- Critical Path
- Challenges and Solutions

# Game Levels (Repeated)
- Synopsis
- Introduction
- Physical Description
- Objectives
- Level Flow
- Critical Path
- Optional Paths
- Important Challenges and Solutions
- Trivial Challenges and Solutions
- Level Conclusion(s)

# User Interfaces
- Control Systems
- Cameras
- Menu Systems
- HUDs and Minimaps
- Help Systems
- UI Visuals and Audio

---


# Game Engine
- Game Engine and Version
- Relevant Version Notes
- Highest Engine Upgrade Possible

# Game Programming
- Programming Languages Used
- In-House Plugins and Scripts
- Third-party Plugins and Scripts
- Version List and Upgradability for Everything

# Game AI
- AI Overview and Methods
- Friendly AI
- Neutral AI
- Hostile AI (Out of Combat)
- Hostile AI (In Combat)

# Game Art
- Art Style
- Visual Themes
- Concept Art
- Environments
- Characters
- Objects
- Miscellaneous

# Game Audio
- Aural Theme
- Background Music
- Environmental Audio
- Sound Effects
- Menu Sounds
- Voice Acting

---

# Technical Specs

- Target Hardware
- FPS on Test Systems (Late Beta to Gold)
- External Libraries (Visual Studio, Etc)

# Design Plan
- Team Members and Roles
- Production Phases (POC to Gold)
- Testing Milestones
- Minimum For Release
- Early Access Plan
- Marketing Milestones

# Installation and Update Plan
- Installation Methods
- External Install Software
- Upgrade Methods
- Roll-Back Versions?
- Beta Versions?

# Proposed Schedule
- POC to Alpha
- Alpha to Beta
- Beta to Gold
- Gold to Release
- Post-Release

# Asset List
- Art and Animation List
- Audio and UI Sounds List
- Special Effects List
- Code and Plugin List
- Full VA Scripts (Links are OK!)
