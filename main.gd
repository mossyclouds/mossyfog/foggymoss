## MainGame is the main class for which to... okay wait a second.
##[br]Basically, yes.

## @tool

## class_name
class_name MainGame

## extends
extends Node
## # docstring
# Testing the Docstring ~~~

## signals
signal user_became_idle()

## enums
enum PauseState {PAUSED, MID_PAUSE, UNPAUSED}
enum UserState {
	USER_INIT,
	USER_READY,
	USER_IDLE,
	USER_ACTIVE,
}
enum DebugMode {NONE, CUSTOM, ALL}

## constants
const test : float =  0.00

## exported variables
@export var show_debug : bool = false

## public variables
var user_state : UserState

## private variables
var _idle_user : bool = false

## onready variables
@onready var user_ready : bool = true

## optional built-in virtual _init method
func _init() -> void:
	user_state = UserState.USER_INIT


## built-in virtual _ready method
func _ready() -> void:
	user_state = UserState.USER_READY


## remaining built-in virtual methods
func _process(delta: float) -> void:
	pass


## public methods
## The do_something method for this plugin. before using the
## method you first have to initialize [MyPlugin].
## see : [method initialize]
## [color=yellow]Warning:[/color] always [method clean] after use.
## Usage:
##     [codeblock]
##     func _ready():
##         the_plugin.initialize()
##         the_plugin.do_something()
##         the_plugin.clean()
##     [/codeblock]
func my_public_method() -> void:
	pass


## private methods
func _on_main_user_became_idle() -> void:
	pass


